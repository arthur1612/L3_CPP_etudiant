cmake_minimum_required (VERSION 3.0)
project (TP1)

add_executable( main_fibonacci.out main.cpp fibonacci.cpp vecteur3.cpp)

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )

add_executable( main_test.out
    main_test.cpp fibonacci.cpp fibonacci_test.cpp)
target_link_libraries( main_test.out
    ${PKG_CPPUTEST_LIBRARIES} )
