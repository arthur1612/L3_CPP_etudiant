#include <iostream>
#include "fibonacci.hpp"
#include "vecteur3.hpp"

int main (){
    /*std::cout << fibonacciRecursif(7) << std::endl;
    std::cout << fibonacciIteratif(7) << std::endl;*/

    std::cout<<"Vecteur initial créé" << std::endl;
    std::cout << "Coordonnées : " << std::endl;
    vecteur3 vecteur(2.0, 3.0, 6.0);
    vecteur.afficherVecteur();
    std::cout << "Norme de ce vecteur : " <<vecteur.calculerNorme()<< std::endl;

    std::cout<<"Vecteur A de coordonnéees : 5.6, 7, 10.9 créé" << std::endl;
    vecteur3 vecteur_a(5.6, 7.0, 10.9);

    std::cout<<"Vecteur B de coordonnéees : 1.5, 6.4, 9 créé" << std::endl;
    vecteur3 vecteur_b(1.5, 6.4, 9.0);

    std::cout << "Produit scalaire des vecteurs A et B : " << vecteur.produitScalaire(vecteur_a, vecteur_b)<< std::endl;

    vecteur3 vecteur_add = vecteur.addition(vecteur_a, vecteur_b);
    std::cout << "Addition des vecteurs A et B || vecteur résultant de coordonnées : " << std::endl;
    vecteur_add.afficherVecteur();

    return 1;
}
