#include "fibonacci.hpp"

int fibonacciRecursif(int nb){
	if (nb < 2)
		return nb;
	else
		return fibonacciRecursif(nb-1) + fibonacciRecursif(nb-2);
}

int fibonacciIteratif(int nb){
	int u = 0;
	int v = 1;
	int i, t;

	for(i = 2; i <= nb; i++) {
		t = u + v;
		u = v;
		v = t;
	}
	return v;
}
