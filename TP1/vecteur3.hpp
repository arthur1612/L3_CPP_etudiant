#ifndef VECTEUR3_HPP
#define VECTEUR3_HPP

struct Vecteur{
    float x;
    float y;
    float z;
};

class vecteur3
{
    private:
        Vecteur vecteur;
    public:
        vecteur3(float x, float y, float z);
        void afficherVecteur();
        float calculerNorme();
        float produitScalaire(vecteur3 vecteur_a, vecteur3 vecteur_b);
        vecteur3 addition (vecteur3 vecteur_a, vecteur3 vecteur_b);
};

#endif // VECTEUR3_HPP
